using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Follow : MonoBehaviour
{
    public GameObject Mani;
    public GameObject Aseguir;
    public float speed;
    public int lifeMani;
    private Vector3 posInicial;
    public bool desapareci;
    
    void Start()
    {
        posInicial = transform.position;
    }

    
    void Update()
    {
        Mani.transform.position = Vector3.MoveTowards(Mani.transform.position, Aseguir.transform.position, speed);
    }

    public void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Agua"))
        {
            lifeMani--;
        }
        if (lifeMani == 0)
        {
            Mani.SetActive(false);
           
        }
    }

    public void Volver()
    {
        transform.position = posInicial;
    }

  
}
