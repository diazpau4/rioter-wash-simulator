using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarraDeVida : MonoBehaviour
{

    public Image barraCongreso;
    public Image barraTanque;

    public float vidaMaxima;
    public float vidaCongreMax;

    public MoveTank tanqueScript;
    public Congreso congresoScript;

    void Update()
    {
        barraTanque.fillAmount = tanqueScript.lifeTank / vidaMaxima;
        barraCongreso.fillAmount = congresoScript.CongresoLife / vidaCongreMax;
    }




}
