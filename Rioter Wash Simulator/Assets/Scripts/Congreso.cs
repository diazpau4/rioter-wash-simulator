using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Congreso : MonoBehaviour
{
    public int CongresoLife = 200;
    void Start()
    {
        
    }

    
    void Update()
    {
        
    }

    public void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Enemy"))
        {
            CongresoLife--;
            collision.gameObject.GetComponent<Follow>().Volver();
        }

        if (CongresoLife == 0)
        {
            SceneManager.LoadScene("GameOver");
        }
    }
}
